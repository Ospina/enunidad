defmodule EnUnidadWeb.Schema do
  use Absinthe.Schema

  alias EnUnidadWeb.Resolvers

  import_types __MODULE__.AccountTypes
  import_types __MODULE__.AdministrationTypes
  import_types __MODULE__.PlaceTypes
  import_types __MODULE__.LobbyTypes

  query do
    # Other query fields
    field :guests, list_of(:guest) do
      resolve &Resolvers.Lobby.lobby_guests/3
    end
  end



  mutation do
    @desc "Create a new guest"
    field :create_guest, :guest_result do
      arg :input, non_null(:guest_input)
      resolve &Resolvers.Lobby.create_guest/3
    end

  end


  @desc "An error encountered trying to persist input"
  object :input_error do
    field :key, non_null(:string)
    field :message, non_null(:string)
  end


  scalar :date do
    parse fn input ->
      with %Absinthe.Blueprint.Input.String{value: value} <- input,
      {:ok, date} <- Date.from_iso8601(value) do
        {:ok, date}
      else
        _ -> :error
      end
    end

    serialize fn date ->
      Date.to_iso8601(date)
    end
  end

  enum :sort_order do
    value :asc
    value :desc
  end

end
