defmodule EnUnidadWeb.Router do
  use EnUnidadWeb, :router

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/" do
    pipe_through :api

    forward "/api", Absinthe.Plug,
      schema: EnUnidadWeb.Schema

    forward "/graphiql", Absinthe.Plug.GraphiQL,
      schema: EnUnidadWeb.Schema,
      interface: :simple # :advanced
  end

end
