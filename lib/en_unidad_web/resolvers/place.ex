defmodule EnUnidadWeb.Resolvers.Place do
  alias EnUnidad.Place

  def place_residences(_, _, _) do
    {:ok, Place.list_residences()}
  end

  def create_residence(_, %{input: params}, _) do
    with {:ok, residence} <- Place.create_residence(params) do
      {:ok, %{place_residence: residence}}
    end
  end


  def place_apartments(_, _, _) do
    {:ok, Place.list_apartments()}
  end

  def create_apartment(_, %{input: params}, _) do
    with {:ok, apartment} <- Place.create_apartment(params) do
      {:ok, %{place_apartment: apartment}}
    end
  end

end
