defmodule EnUnidadWeb.Resolvers.Administration do
  alias EnUnidad.Administration

  def administration_news(_, _, _) do
    {:ok, Administration.list_news()}
  end

  def create_news(_, %{input: params}, _) do
    with {:ok, news} <- Administration.create_news(params) do
      {:ok, %{administration_news: news}}
    end
  end

end
