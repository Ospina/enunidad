defmodule EnUnidadWeb.Resolvers.Account do
  alias EnUnidad.Account

  def account_users(_, _, _) do
    {:ok, Account.list_users()}
  end

  def create_user(_, %{input: params}, _) do
    with {:ok, user} <- Account.create_user(params) do
      {:ok, %{account_user: user}}
    end
  end

end
