defmodule EnUnidadWeb.Resolvers.Lobby do
  alias EnUnidad.Lobby

  def lobby_guests(_, _, _) do
    {:ok, Lobby.list_guests()}
  end

  def create_guest(_, %{input: params}, _) do
    with {:ok, guest} <- Lobby.create_guest(params) do
      {:ok, %{lobby_guest: guest}}
    end
  end


  def lobby_packages(_, _, _) do
    {:ok, Lobby.list_packages()}
  end

  def create_package(_, %{input: params}, _) do
    with {:ok, package} <- Lobby.create_package(params) do
      {:ok, %{lobby_package: package}}
    end
  end

end
