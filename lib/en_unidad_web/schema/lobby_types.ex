defmodule EnUnidadWeb.Schema.LobbyTypes do
  use Absinthe.Schema.Notation

  alias EnUnidadWeb.Resolvers

  object :guest do
    #field :id, :id
    field :name, :string
    field :type, :string
    field :apartment_id, :apartment
    field :description, :string
  end

  object :guest_result do
    field :guest, :guest
    field :errors, list_of(:input_error)
  end

  input_object :guest_input do
    field :name, non_null(:string)
    field :type, non_null(:string)
    field :description, :string
  end


  object :package do
    #field :id, :id
    field :curier, :string
    field :type, :string
  end

  object :package_result do
    field :package, :package
    field :errors, list_of(:input_error)
  end

  input_object :package_input do
    field :curier, :string
    field :type, non_null(:string)
    field :description, :string
  end


end
