defmodule EnUnidadWeb.Schema.AdministrationTypes do
  use Absinthe.Schema.Notation

  alias EnUnidadWeb.Resolvers

  object :news do
    field :title, :string
    field :body, :string
    field :picture, :string
    field :likes, :integer
    field :dislikes, :integer
    field :residence, :residence
    field :user, :user
  end

  object :news_result do
    field :news, :news
    field :errors, list_of(:input_error)
  end

  input_object :news_input do
    field :title, non_null(:string)
    field :body, non_null(:string)
    field :residence, :residence
    field :user, :user
  end


end
