defmodule EnUnidadWeb.Schema.AccountTypes do
  use Absinthe.Schema.Notation

  alias EnUnidadWeb.Resolvers

  object :user do
    field :phone, :string
    field :username, :string
    field :first_name, :string
    field :last_name, :string
    field :email, :string
    field :apartments, list_of(:apartment)
  end

  object :user_result do
    field :user, :user
    field :errors, list_of(:input_error)
  end

  input_object :user_input do
    field :phone, non_null(:string)
    field :username, :string
    field :first_name, :string
    field :last_name, :string
    field :email, :string
  end


end
