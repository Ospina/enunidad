defmodule EnUnidadWeb.Schema.PlaceTypes do
  use Absinthe.Schema.Notation

  alias EnUnidadWeb.Resolvers

  object :apartment do
    #field :id, :id
    field :number, :string
    field :tower, :string
    field :convention, :string
    field :residence, :residence
    field :guests, list_of(:guest)
    field :packages, list_of(:package)
  end

  object :apartment_result do
    field :apartment, :apartment
    field :errors, list_of(:input_error)
  end

  input_object :apartment_input do
    field :number, :string
    field :tower, :string
    field :convention, :string
    field :residence, non_null(:residence)
    field :description, :string
  end


  object :residence do
    #field :id, :id
    field :name, :string
    field :city, :string
    field :address, :string
    field :has_payment, :integer
    field :news, list_of(:news)
    field :apartments, list_of(:apartment)
  end

  object :residence_result do
    field :residence, :residence
    field :errors, list_of(:input_error)
  end

  input_object :residence_input do
    field :name, :string
    field :city, :string
    field :address, :string
    field :has_payment, :integer
  end


end
