defmodule EnUnidad.Place.Residence do
  use Ecto.Schema
  import Ecto.Changeset
  alias EnUnidad.Place.Residence


  schema "residences" do
    field :name, :string
    field :city, :string
    field :address, :string
    field :has_payment, :integer

    has_many :news, EnUnidad.Administration.News
    has_many :apartments, EnUnidad.Place.Apartment

    timestamps()
  end

  @doc false
  def changeset(%Residence{} = residence, attrs) do
    residence
    |> cast(attrs, [:number, :convention, :tower])
    |> validate_required([:number, :convention])
  end
end
