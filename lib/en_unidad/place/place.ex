defmodule EnUnidad.Place do
  @moduledoc """
  The Place context.
  """

  import Ecto.Query, warn: false
  alias EnUnidad.Repo

  alias EnUnidad.Place.Apartment

  @doc """
  Returns the list of apartments.

  ## Examples

      iex> list_apartments()
      [%Apartment{}, ...]

  """
  def list_apartments do
    Repo.all(Apartment)
  end

  @doc """
  Gets a single apartment.

  Raises `Ecto.NoResultsError` if the apartment does not exist.

  ## Examples

      iex> get_apartment!(123)
      %Apartment{}

      iex> get_apartment!(456)
      ** (Ecto.NoResultsError)

  """
  def get_apartment!(id), do: Repo.get!(Apartment, id)

  @doc """
  Creates a apartment.

  ## Examples

      iex> create_apartment(%{field: value})
      {:ok, %Apartment{}}

      iex> create_apartment(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_apartment(attrs \\ %{}) do
    %Apartment{}
    |> Apartment.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a apartment.

  ## Examples

      iex> update_apartment(apartment, %{field: new_value})
      {:ok, %Apartment{}}

      iex> update_apartment(apartment, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_apartment(%Apartment{} = apartment, attrs) do
    apartment
    |> Apartment.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a apartment.

  ## Examples

      iex> delete_apartment(apartment)
      {:ok, %Apartment{}}

      iex> delete_apartment(apartment)
      {:error, %Ecto.Changeset{}}

  """
  def delete_apartment(%Apartment{} = apartment) do
    Repo.delete(apartment)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking apartment changes.

  ## Examples

      iex> change_apartment(apartment)
      %Ecto.Changeset{source: %Apartment{}}

  """
  def change_apartment(%Apartment{} = apartment) do
    Apartment.changeset(apartment, %{})
  end




  alias EnUnidad.Place.Residence

  @doc """
  Returns the list of residences.

  ## Examples

      iex> list_residences()
      [%Residence{}, ...]

  """
  def list_residences do
    Repo.all(Residence)
  end

  @doc """
  Gets a single residence.

  Raises `Ecto.NoResultsError` if the residence does not exist.

  ## Examples

      iex> get_residence!(123)
      %Residence{}

      iex> get_residence!(456)
      ** (Ecto.NoResultsError)

  """
  def get_residence!(id), do: Repo.get!(Residence, id)

  @doc """
  Creates a residence.

  ## Examples

      iex> create_residence(%{field: value})
      {:ok, %Residence{}}

      iex> create_residence(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_residence(attrs \\ %{}) do
    %Residence{}
    |> Residence.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a residence.

  ## Examples

      iex> update_residence(residence, %{field: new_value})
      {:ok, %Residence{}}

      iex> update_residence(residence, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_residence(%Residence{} = residence, attrs) do
    residence
    |> Residence.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a residence.

  ## Examples

      iex> delete_residence(residence)
      {:ok, %Residence{}}

      iex> delete_residence(residence)
      {:error, %Ecto.Changeset{}}

  """
  def delete_residence(%Residence{} = residence) do
    Repo.delete(residence)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking residence changes.

  ## Examples

      iex> change_residence(residence)
      %Ecto.Changeset{source: %Residence{}}

  """
  def change_residence(%Residence{} = residence) do
    Residence.changeset(residence, %{})
  end

end