defmodule EnUnidad.Place.Apartment do
  use Ecto.Schema
  import Ecto.Changeset
  alias EnUnidad.Place.Apartment


  schema "apartments" do
    field :number, :string
    field :tower, :string
    field :convention, :string

    belongs_to :residence, EnUnidad.Place.Residence
    has_many :guests, EnUnidad.Lobby.Guest
    has_many :packages, EnUnidad.Lobby.Package
    many_to_many :users, EnUnidad.Account.User, join_through: "user_apartments"


    timestamps()
  end

  @doc false
  def changeset(%Apartment{} = apartament, attrs) do
    apartament
    |> cast(attrs, [:number, :convention, :tower])
    |> validate_required([:number, :convention])
    |> foreign_key_constraint(:residence)
  end
end
