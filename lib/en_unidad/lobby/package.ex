defmodule EnUnidad.Lobby.Package do
  use Ecto.Schema
  import Ecto.Changeset
  alias EnUnidad.Lobby.Package


  schema "packages" do
    field :curier, :string
    field :type, :string

    belongs_to :apartment, EnUnidad.Place.Apartment

    timestamps()
  end

  @doc false
  def changeset(%Package{} = package, attrs) do
    package
    |> cast(attrs, [:type, :curier])
    |> validate_required([:type])
    |> foreign_key_constraint(:apartment)
  end
end
