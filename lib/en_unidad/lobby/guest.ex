defmodule EnUnidad.Lobby.Guest do
  use Ecto.Schema
  import Ecto.Changeset
  alias EnUnidad.Lobby.Guest


  schema "guests" do
    field :name, :string
    field :type, :string
    field :plate, :string
    field :status, :string
    field :place, :string
    field :description, :string

    belongs_to :apartment, EnUnidad.Place.Apartment

    timestamps()
  end

  @doc false
  def changeset(%Guest{} = guest, attrs) do
    guest
    |> cast(attrs, [:name, :type, :plate, :status, :place, :description])
    |> validate_required([:type])
    |> foreign_key_constraint(:apartment)
  end
end
