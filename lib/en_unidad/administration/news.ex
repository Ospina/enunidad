defmodule EnUnidad.Administration.News do
  use Ecto.Schema
  import Ecto.Changeset
  alias EnUnidad.Administration.News


  schema "news" do
    field :title, :string
    field :body, :string
    field :picture, :string
    field :likes, :integer
    field :dislikes, :integer

    belongs_to :residence, EnUnidad.Place.Residence
    belongs_to :user, EnUnidad.Account.User

    timestamps()
  end

  @doc false
  def changeset(%News{} = news, attrs) do
    news
    |> cast(attrs, [:title, :body, :picture, :likes, :dislikes])
    |> validate_required([:title, :body])
    |> foreign_key_constraint(:residence)
    |> foreign_key_constraint(:user)
  end
end
