defmodule EnUnidad.Account.Session do
  use Ecto.Schema
  import Ecto.Changeset
  alias EnUnidad.Account.Session


  schema "sessions" do
    field :token, :string
    field :ip, :string
    field :device, :string

    belongs_to :user, EnUnidad.Account.User

    timestamps()
  end

  @doc false
  def changeset(%Session{} = session, attrs) do
    session
    |> cast(attrs, [:token, :ip, :device, :user_id])
    |> validate_required([:token])
    |> foreign_key_constraint(:user)
  end
end
