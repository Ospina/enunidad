defmodule EnUnidad.Account.User do
  use Ecto.Schema
  import Ecto.Changeset
  alias EnUnidad.Account.User


  schema "users" do
    field :phone, :string
    field :username, :string
    field :first_name, :string
    field :last_name, :string
    field :email, :string

    many_to_many :apartment, EnUnidad.Place.Apartment, join_through: "user_apartments"

    timestamps()
  end

  @doc false
  def changeset(%User{} = user, attrs) do
    user
    |> cast(attrs, [:phone, :username, :first_name, :last_name, :email])
    |> validate_required([:phone])
  end
end
