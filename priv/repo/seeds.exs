#---
# Excerpted from "Craft GraphQL APIs in Elixir with Absinthe",
# published by The Pragmatic Bookshelf.
# Copyrights apply to this code. It may not be used to create training material,
# courses, books, articles, and the like. Contact us if you are in doubt.
# We make no guarantees that this code is fit for any purpose.
# Visit http://www.pragmaticprogrammer.com/titles/wwgraphql for more book information.
#---
# Script for populating the database. You can run it as:
#
#     mix run priv/repo/seeds.exs
#
# Inside the script, you can read and write to any of your
# repositories directly:
#
#     EnUnidad.Repo.insert!(%EnUnidad.SomeSchema{})
#
# We recommend using the bang functions (`insert!`, `update!`
# and so on) as they will fail if something goes wrong.

alias EnUnidad.{Account, Lobby, Place, Administration, Repo}


#
# RESIDENCES
#
residence1 =
  %Place.Residence{
    name: "Viña del Mar",
    city: "La Estrella",
    address: "Carrea 77 S",
    has_payment: 1
  }
  |> Repo.insert!

residence2 =
  %Place.Residence{
    name: "Entre Bosques",
    city: "Itagüí",
    address: "NA",
    has_payment: 0
  }
  |> Repo.insert!


#
# APARTMENTS
#
apt1 =
  %Place.Apartment{
    number: "523",
    tower: "6",
    convention: "Apartamento",
    residence: residence1
  }
  |> Repo.insert!

apt2 =
  %Place.Apartment{
    number: "407",
    convention: "Casa",
    residence: residence2
  }
  |> Repo.insert!

apt3 =
  %Place.Apartment{
    number: "407",
    convention: "Apartamento",
    residence: residence1
  }
  |> Repo.insert!

#
# USERS
#
user1 =
  %Account.User{
    phone: "573007180821",
    username: "CristianOspina",
    first_name: "Cristian",
    last_name: "Ospina",
    email: "ospina_522@hotmail.com",
    apartment: [apt1]
  }
  |> Repo.insert!

user2 =
  %Account.User{
    phone: "573007180822",
    username: "EvilMinds",
    first_name: "Evil",
    last_name: "Minds",
    email: "EvilMinds@outlook.com",
    apartment: [apt2]
  }
  |> Repo.insert!
