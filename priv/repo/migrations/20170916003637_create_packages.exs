defmodule EnUnidad.Repo.Migrations.CreatePackages do
  use Ecto.Migration

  def change do
    create table(:packages) do
      add :curier, :string
      add :type, :string
      add :apartment_id, references(:apartments, on_delete: :delete_all), null: false

      timestamps()
    end
    create index(:packages, [:apartment_id])
  end
end
