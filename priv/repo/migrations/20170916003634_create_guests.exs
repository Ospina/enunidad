defmodule EnUnidad.Repo.Migrations.CreateGuests do
  use Ecto.Migration

  def change do
    create table(:guests) do
      add :name, :string
      add :type, :string
      add :plate, :string
      add :status, :string
      add :place, :string
      add :description, :string
      add :apartment_id, references(:apartments, on_delete: :delete_all), null: false

      timestamps()
    end
    create index(:guests, [:apartment_id])
  end
end
