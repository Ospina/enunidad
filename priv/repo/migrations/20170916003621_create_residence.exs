defmodule EnUnidad.Repo.Migrations.CreateResidence do
  use Ecto.Migration

  def change do
    create table(:residences) do
      add :name, :string, size: 40
      add :city, :string
      add :address, :string, size: 40
      add :has_payment, :integer

      timestamps()
    end
  end
end
