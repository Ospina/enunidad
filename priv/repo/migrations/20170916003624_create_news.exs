defmodule EnUnidad.Repo.Migrations.CreateNews do
  use Ecto.Migration

  def change do
    create table(:news) do
      add :title, :string, size: 70, null: false
      add :body, :string, null: false
      add :picture, :string
      add :likes, :integer, default: 0
      add :dislikes, :integer, default: 0
      add :residence_id, references(:residences, on_delete: :delete_all), null: false
      add :author, references(:users, on_delete: :delete_all), null: false

      timestamps()
    end
    create index(:news, [:residence_id])
  end
end
