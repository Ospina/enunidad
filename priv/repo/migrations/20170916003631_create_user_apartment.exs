defmodule EnUnidad.Repo.Migrations.CreateUserApartment do
  use Ecto.Migration

  def change do
    create table(:user_apartments, primary_key: false) do
      add :user_id, references(:users, on_delete: :delete_all), null: false
      add :apartment_id, references(:apartments, on_delete: :delete_all), null: false

    end
  end
end
