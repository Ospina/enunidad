defmodule EnUnidad.Repo.Migrations.CreateApartment do
  use Ecto.Migration

  def change do
    create table(:apartments) do
      add :number, :string
      add :tower, :string
      add :convention, :string
      add :residence_id, references(:residences, on_delete: :delete_all), null: false

      timestamps()
    end
  end
end
