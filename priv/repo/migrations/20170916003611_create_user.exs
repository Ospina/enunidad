defmodule EnUnidad.Repo.Migrations.CreateUser do
  use Ecto.Migration

  def change do
    create table(:users) do
      add :phone, :string, size: 15, null: false
      add :username, :string, size: 30
      add :first_name, :string, size: 15
      add :last_name, :string, size: 15
      add :email, :string, size: 30

      timestamps()
    end
    create index(:users, [:phone])
  end
end
